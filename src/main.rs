extern crate lettre;
extern crate lettre_email;
extern crate native_tls;

use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::smtp::ConnectionReuseParameters;
use lettre::{
    ClientSecurity, ClientTlsParameters, EmailAddress, Envelope, SendableEmail, SmtpClient,
    Transport,
};
use native_tls::{Protocol, TlsConnector};

fn main() {
    let mut tls_builder = TlsConnector::builder();
    tls_builder.min_protocol_version(Some(Protocol::Tlsv12));
    let message = [
        "Content-Type: text/html; charset=UTF-8",
        // Here you do a format!("Subject: {}", subject)
        "Subject: Amazon SES Test (Ruby Net::SMTP library)",
        "",
        "<h1> Howdy </h1>", // Here you stick the content or body
    ]
    .join("\n");

    let tls_parameters = ClientTlsParameters::new(
        "email-smtp.us-east-1.amazonaws.com".to_string(),
        tls_builder.build().unwrap(),
    );
    let email = SendableEmail::new(
        Envelope::new(
            Some(EmailAddress::new("verify@lendsmart.ai".to_string()).unwrap()),
            vec![EmailAddress::new("kishore@lendsmart.ai".to_string()).unwrap()],
        )
        .unwrap(),
        "message_id".to_string(),
        message.into_bytes(),
    );
    let mut mailer = SmtpClient::new(
        ("email-smtp.us-east-1.amazonaws.com", 465),
        ClientSecurity::Wrapper(tls_parameters),
    )
    .unwrap()
    .authentication_mechanism(Mechanism::Login)
    .credentials(Credentials::new(
        "smtp_username".to_string(),
        "smtp_password".to_string(),
    ))
    .connection_reuse(ConnectionReuseParameters::ReuseUnlimited)
    .transport();

    // Send the email
    match mailer.send(email) {
        Ok(_) => println!("â†’ SENT"),
        Err(e) => println!("â†’ {:?} FAIL", e),
    };

    mailer.close();
}
